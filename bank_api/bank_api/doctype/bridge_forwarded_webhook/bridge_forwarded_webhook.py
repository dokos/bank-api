# Copyright (c) 2025, Dokos SAS and contributors
# For license information, please see license.txt

import json
import requests

import frappe
from frappe.model.document import Document

class BridgeForwardedWebhook(Document):
	def after_insert(self):
		if self.webhook_type == "validate_item":
			self.forward_bridge_webhook()


	def forward_bridge_webhook(self):
		params = {
			"webhook_type": self.webhook_type
		}
		if self.bridge_item_request:
			params.update({
				"request": self.bridge_item_request
			})
		if self.bridge_item:
			params.update({
				"item_id": self.bridge_item
			})

		webhook_url = self.webhook_url or f"{self.base_url}/api/method/bank.api.bridge.webhook"
		try:
			requests.post(
				webhook_url,
				data=json.dumps(params),
				headers={
					"Content-Type": "application/json"
				}
			)
			self.db_set("status", "Sent")
		except requests.exceptions.ConnectionError:
			self.log_error(title="Bridge Webhook Error", message=webhook_url)


def forward_webhooks():
	webhooks = frappe.get_all("Bridge Forwarded Webhook", filters={"status": "Pending"}, fields=["bridge_user", "bridge_item", "name"])
	sent_hooks = set()
	for webhook in webhooks:
		if (webhook.bridge_user, webhook.bridge_item) not in sent_hooks:
			frappe.get_doc("Bridge Forwarded Webhook", webhook.name).run_method("forward_bridge_webhook")
			sent_hooks.add((webhook.bridge_user, webhook.bridge_item))
		else:
			frappe.db.set_value("Bridge Forwarded Webhook", webhook.name, "status", "Sent")
