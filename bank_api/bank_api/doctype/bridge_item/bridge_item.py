# Copyright (c) 2022, DOKOS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

from bank_api.bank_api.doctype.bridge_settings.api.item import BridgeAPIItem

class BridgeItem(Document):
	def on_update(self):
		self.get_item_details()

	def on_trash(self):
		for webhook in frappe.get_all("Bridge Forwarded Webhook", filters={"bridge_item": self.name}):
			frappe.delete_doc("Bridge Forwarded Webhook", webhook.name)

		bridge_user = frappe.get_doc("Bridge User", self.bridge_user)

		token = bridge_user.authenticate()

		BridgeAPIItem(token=token).delete_item(self.item_id)

	def get_item_details(self):
		bridge_user = frappe.get_doc("Bridge User", self.bridge_user)

		token = bridge_user.authenticate()

		res = BridgeAPIItem(token=token).get_single(self.item_id)

		if not self.bank_id or self.bank_id != res.get("bank_id"):
			self.bank_id = res.get("bank_id")
			self.db_set("bank_id", res.get("bank_id"))

		if self.status_code != res.get("status"):
			self.status_code = res.get("status")
			self.db_set("status_code", res.get("status"))

		if self.status_code_info != res.get("status_code_info"):
			self.status_code_info = res.get("status_code_info")
			self.db_set("status_code_info", res.get("status_code_info"))
