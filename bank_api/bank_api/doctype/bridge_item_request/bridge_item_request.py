# Copyright (c) 2022, DOKOS and contributors
# For license information, please see license.txt
import frappe
from frappe.model.document import Document

from bank_api.bank_api.doctype.bridge_settings.api.item import BridgeAPIItem

class BridgeItemRequest(Document):
	def before_insert(self):
		for request in frappe.get_all("Bridge Item Request", filters={
			"bridge_user": self.bridge_user,
			"bank_name": self.bank_name,
			"status": "Pending"
		}):
			frappe.db.set_value("Bridge Item Request", request.name, "status", "Abandonned")

	def after_insert(self):
		if self.webhook_url != frappe.db.get_value("Bridge User", self.bridge_user, "webhook_url"):
			frappe.db.set_value("Bridge User", self.bridge_user, "webhook_url", self.webhook_url)

	def on_update(self):
		if self.item_id:
			frappe.get_doc({
				"doctype": "Bridge Item",
				"item_id": self.item_id,
				"bridge_user": self.bridge_user,
				"item_request": self.name
			}).insert(ignore_permissions=True, ignore_if_duplicate=True)

	def get_bridge_url(self):
		bridge_user = frappe.get_doc("Bridge User", self.bridge_user)

		token = bridge_user.authenticate()

		if self.request_type == "Connect":
			res = BridgeAPIItem(token=token).add(
				data=dict(context=self.name)
			)

		elif self.request_type == "Edit":
			res = BridgeAPIItem(token=token).edit(
				params=dict(
					item_id=self.item_id,
					context=self.name
				)
			)

		elif self.request_type == "Sync":
			res = BridgeAPIItem(token=token).sync(
				params=dict(
					item_id=self.item_id,
					context=self.name
				)
			)

		elif self.request_type == "Pro":
			res = BridgeAPIItem(token=token).pro_confirmation(
				params=dict(
					context=self.name
				)
			)

		if res.get("type") in ("forbidden", "missing_authorization_header"):
			frappe.log_error(res.get("message") or str(res), "Forbidden Bridge Request")

		return res

	def callback_site(self):
		forwarded_webhook = frappe.new_doc("Bridge Forwarded Webhook")
		forwarded_webhook.bridge_user = self.bridge_user
		forwarded_webhook.bridge_item = self.item_id
		forwarded_webhook.bridge_item_request = self.name
		forwarded_webhook.base_url = self.base_url
		forwarded_webhook.webhook_url = frappe.db.get_value("Bridge User", self.bridge_user, "webhook_url")
		forwarded_webhook.webhook_type = "validate_item"
		forwarded_webhook.insert()