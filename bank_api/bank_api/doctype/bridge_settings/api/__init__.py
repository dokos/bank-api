import json
import requests

import frappe

class BridgeAPI:
	def __init__(self, *args, **kwargs):
		settings = frappe.get_single("Bridge Settings")
		self.headers = {
			"Client-Id": settings.client_id,
			"Client-Secret": settings.get_password(fieldname="client_secret"),
			"Bridge-Version": settings.api_version,
			"Content-Type": "application/json"
		}

		if kwargs.get("token", {}).get("access_token"):
			self.headers.update({
				"Authorization": f"Bearer {kwargs.get('token', {}).get('access_token')}"
			})

		if kwargs.get("lang"):
			self.headers.update({
				"Accept-Language": kwargs.get("lang")
			})

		self.base_url = settings.base_url
		self.session = requests.Session()

	def get(self, **kwargs):
		output = self.session.get(
			self.url,
			headers=self.headers,
			params=kwargs.get("params")
		).json()

		pagination = output.get("pagination", {}).get("next_uri")
		while pagination:
			self.url = f"{self.base_url.rstrip('/')}{pagination}"
			res = self.session.get(
				self.url,
				headers=self.headers
			).json()

			if res.get("resources"):
				output["resources"].extend(res.get("resources"))

			next_pagination = res.get("pagination", {}).get("next_uri")
			pagination = next_pagination if pagination != next_pagination else None

		return output

	def post(self, **kwargs):
		res = self.session.post(
			self.url,
			headers=self.headers,
			data=json.dumps(
				kwargs.get("data")
			),
			params=kwargs.get("params")
		)

		try:
			return res.json()
		except Exception:
			frappe.log_error(res.text, "POST Response error")
			return res

	def put(self, **kwargs):
		return self.session.put(
			self.url,
			headers=self.headers,
			data=json.dumps(
				kwargs.get("data")
			),
			params=kwargs.get("params")
		).json()

	def delete(self):
		return self.session.delete(
			self.url,
			headers=self.headers
		)

