from bank_api.bank_api.doctype.bridge_settings.api import BridgeAPI

class BridgeAPIAccount(BridgeAPI):
	def __init__(self, *args, **kwargs):
		super(BridgeAPIAccount, self).__init__(*args, **kwargs)

		self.url = f"{self.base_url.rstrip('/')}/v2/accounts"

	def get_single(self, id):
		self.url = f"{self.base_url.rstrip('/')}/v2/accounts/{id}"
		return self.get()

	def get_list(self, params=None):
		return self.get(params=params)

	def get_updated_transactions_list(self, id, params=None):
		self.url = f"{self.base_url.rstrip('/')}/v2/accounts/{id}/transactions/updated"
		return self.get(params=params)
