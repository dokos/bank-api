from bank_api.bank_api.doctype.bridge_settings.api import BridgeAPI

class BridgeAPIBank(BridgeAPI):
	def __init__(self, *args, **kwargs):
		super(BridgeAPIBank, self).__init__(*args, **kwargs)

		self.url = f"{self.base_url.rstrip('/')}/v2/banks"

	def get_single(self, id):
		self.url = f"{self.base_url.rstrip('/')}/v2/banks/{id}"
		return self.get()

	def get_list(self, params=None):
		return self.get(params=params)

