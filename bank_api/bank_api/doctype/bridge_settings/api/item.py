from bank_api.bank_api.doctype.bridge_settings.api import BridgeAPI

class BridgeAPIItem(BridgeAPI):
	def __init__(self, *args, **kwargs):
		super(BridgeAPIItem, self).__init__(*args, **kwargs)

		self.url = f"{self.base_url.rstrip('/')}/v2/items"

	def add(self, data=None):
		self.url = f"{self.base_url.rstrip('/')}/v2/connect/items/add"

		return self.post(data=data)

	def edit(self, params=None):
		self.url = f"{self.base_url.rstrip('/')}/v2/connect/items/edit"

		return self.get(params=params)

	def sync(self, params=None):
		self.url = f"{self.base_url.rstrip('/')}/v2/connect/items/sync"

		return self.get(params=params)

	def pro_confirmation(self, params=None):
		self.url = f"{self.base_url.rstrip('/')}/v2/connect/items/pro/confirmation"

		return self.get(params=params)

	def get_single(self, id):
		self.url = f"{self.base_url.rstrip('/')}/v2/items/{id}"
		return self.get()

	def get_list(self, params=None):
		return self.get(params=params)

	def delete_item(self, id):
		self.url = f"{self.base_url.rstrip('/')}/v2/items/{id}"
		return self.delete()

	def refresh(self, id):
		self.url = f"{self.base_url.rstrip('/')}/v2/items/{id}/refresh"

		return self.post()