from bank_api.bank_api.doctype.bridge_settings.api import BridgeAPI

class BridgeAPITransaction(BridgeAPI):
	def __init__(self, *args, **kwargs):
		super(BridgeAPITransaction, self).__init__(*args, **kwargs)

		self.url = f"{self.base_url.rstrip('/')}/v2/transactions"

	def get_single(self, id):
		self.url = f"{self.base_url.rstrip('/')}/v2/transactions/{id}"
		return self.get()

	def get_list(self, params=None):
		return self.get(params=params)

	def get_updated_list(self, params=None):
		self.url = f"{self.base_url.rstrip('/')}/v2/transactions/updated"
		return self.get(params=params)
