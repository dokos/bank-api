from frappe.model.document import Document

from bank_api.bank_api.doctype.bridge_settings.api import BridgeAPI

class BridgeAPIUser(BridgeAPI):
	def __init__(self, *args, **kwargs):
		super(BridgeAPIUser, self).__init__(*args, **kwargs)

		self.url = f"{self.base_url.rstrip('/')}/v2/users"

	def authenticate(self, user:Document):
		self.url = f"{self.base_url.rstrip('/')}/v2/authenticate"
		return self.post(
			data={
				"email": user.email,
				"password": user.get_password()
			}
		)

	def create_user(self, email, password):
		return self.post(
			data={
				"email": email,
				"password": password
			}
		)

	def get_single_user(self, id):
		return self.get(id)

	def get_list(self, params=None):
		if not params:
			params = {}

		return self.get(params=params)

	def delete(self, uuid:str, pwd:str):
		self.url = f"{self.base_url.rstrip('/')}/v2/users/{uuid}/delete"
		return self.post(
			data={
				"password": pwd
			}
		)