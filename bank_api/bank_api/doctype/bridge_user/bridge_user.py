# Copyright (c) 2022, DOKOS and contributors
# For license information, please see license.txt

from frappe.model.document import Document

from bank_api.bank_api.doctype.bridge_settings.api.user import BridgeAPIUser

class BridgeUser(Document):
	def before_insert(self):
		self.create_remote_user()

	def validate(self):
		self.create_remote_user()
		self.set_base_url()

	def on_update(self):
		if self.status == "Deleted" and self.uuid:
			BridgeAPIUser().delete(
				self.uuid,
				self.get_password()
			)
			self.db_set("uuid", None, commit=True)

	def create_remote_user(self):
		if self.status == "Active" and not self.uuid:
			remote_user = BridgeAPIUser()
			response = remote_user.create_user(
				email=self.email,
				password=self.get_password()
			)

			self.uuid = response.get("uuid")

	def authenticate(self):
		return BridgeAPIUser().authenticate(self)

	def set_base_url(self):
		if not self.base_url and self.site:
			self.base_url = f"https://{self.site}"