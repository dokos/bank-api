# Copyright (c) 2022, DOKOS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class BridgeWebhook(Document):
	def after_insert(self):
		self.handle_webhooks()

	def handle_webhooks(self):
		if self.type == "item.created":
			# Not implemented
			pass

		elif self.type == "item.account.created":
			# Not implemented
			pass

		elif self.type in ("item.refreshed", "item.account.updated"):
			return self.forward_webhook_to_user_site()

	def forward_webhook_to_user_site(self):
		content = frappe.parse_json(self.content)
		user = frappe.db.get_value("Bridge User", dict(uuid=content.get("user_uuid")), ["name", "base_url", "webhook_url"], as_dict=True)

		try:
			forwarded_webhook = frappe.new_doc("Bridge Forwarded Webhook")
			forwarded_webhook.bridge_user = user.name
			forwarded_webhook.bridge_item = content.get("item_id")
			forwarded_webhook.base_url = user.base_url
			forwarded_webhook.webhook_url = user.webhook_url
			forwarded_webhook.bridge_webhook = self.name
			forwarded_webhook.webhook_type = self.type
			forwarded_webhook.insert(ignore_permissions=True, ignore_links=True)
		except Exception:
			self.log_error()
