import hmac
import hashlib
import json

import frappe
from frappe import _
from frappe.utils import get_datetime
from frappe.client import get_value

from bank_api.bank_api.doctype.bridge_settings.api.account import BridgeAPIAccount
from bank_api.bank_api.doctype.bridge_settings.api.item import BridgeAPIItem
from bank_api.bank_api.doctype.bridge_settings.api.transaction import BridgeAPITransaction
from bank_api.bank_api.doctype.bridge_settings.api.category import BridgeAPICategory
from bank_api.bank_api.doctype.bridge_settings.api.bank import BridgeAPIBank

def handle_request_errors(func):
	def wrapper(*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except Exception:
			frappe.log_error(
				f"{kwargs}<br>{frappe.get_traceback()}",
				"Bridge API Call Error"
			)
	return wrapper

@frappe.whitelist(allow_guest=True)
def webhooks(*args, **kwargs):
	validate_webhook()

	frappe.get_doc({
		"doctype": "Bridge Webhook",
		"type": kwargs.get("type"),
		"content": json.dumps(kwargs.get("content"), indent=2)
	}).insert(ignore_permissions=True)

def validate_webhook():
	if frappe.local.request_ip not in ["63.32.31.5", "52.215.247.62", "34.249.92.209"]:
		frappe.throw(_("IP not authorized"), frappe.PermissionError)

	settings = frappe.get_single("Bridge Settings")
	secret = settings.get_password(fieldname="webhook_secret")
	signature = hmac.new(
		bytes(secret, "utf-8"),
		msg = frappe.request.data,
		digestmod = hashlib.sha256
	).hexdigest().upper()

	api_sig = frappe.get_request_header('BridgeApi-Signature').split(",")
	sigs = []
	for sig in api_sig:
		sigs.append(sig.split("=")[1])

	if signature not in sigs:
		frappe.throw(_("Signature not recognized"), frappe.PermissionError)

@frappe.whitelist()
@handle_request_errors
def get_bridge_connect_link(**kwargs):
	if not kwargs.get("base_url"):
		frappe.throw("base_url is required")

	if not kwargs.get("bank"):
		frappe.throw("bank is required")

	if not kwargs.get("redirect_url"):
		frappe.throw("redirect_url is required")

	return get_request_link(
		base_url=kwargs.get("base_url"),
		bank=kwargs.get("bank"),
		redirect_url=kwargs.get("redirect_url"),
		request_type="Connect",
		item_id=kwargs.get("item_id"),
		site=kwargs.get("site"),
		user=kwargs.get("user"),
		webhook_url=kwargs.get("webhook_url")
	)

@frappe.whitelist()
@handle_request_errors
def get_bridge_edit_link(**kwargs):
	item_id = kwargs.get("item_id")
	if not item_id:
		frappe.throw(_("Item ID is mandatory"))

	if not kwargs.get("base_url"):
		frappe.throw("base_url is required")

	if not kwargs.get("bank"):
		frappe.throw("bank is required")

	if not kwargs.get("redirect_url"):
		frappe.throw("redirect_url is required")

	# TODO: Store user in bank to activate this validation
	# if not kwargs.get("user"):
	# 	frappe.throw("user is required")

	return get_request_link(
		base_url=kwargs.get("base_url"),
		bank=kwargs.get("bank"),
		redirect_url=kwargs.get("redirect_url"),
		request_type="Edit",
		item_id=item_id,
		site=kwargs.get("site"),
		user=kwargs.get("user"),
		webhook_url=kwargs.get("webhook_url")
	)

@frappe.whitelist()
@handle_request_errors
def get_sca_authentication_link(**kwargs):
	if not (item_id:= kwargs.get("item_id")):
		frappe.throw(_("Item ID is mandatory"))

	if not kwargs.get("base_url"):
		frappe.throw("base_url is required")

	if not kwargs.get("bank"):
		frappe.throw("bank is required")

	if not kwargs.get("redirect_url"):
		frappe.throw("redirect_url is required")

	# TODO: Store user in bank to activate this validation
	# if not kwargs.get("user"):
	# 	frappe.throw("user is required")

	return get_request_link(
		base_url=kwargs.get("base_url"),
		bank=kwargs.get("bank"),
		redirect_url=kwargs.get("redirect_url"),
		request_type="Sync",
		item_id=item_id,
		site=kwargs.get("site"),
		user=kwargs.get("user"),
		webhook_url=kwargs.get("webhook_url")
	)

@frappe.whitelist()
@handle_request_errors
def get_pro_confirmation_link(**kwargs):
	item_id = kwargs.get("item_id")
	if not item_id:
		frappe.throw(_("Item ID is mandatory"))

	if not kwargs.get("base_url"):
		frappe.throw("base_url is required")

	if not kwargs.get("bank"):
		frappe.throw("bank is required")

	if not kwargs.get("redirect_url"):
		frappe.throw("redirect_url is required")

	return get_request_link(
		base_url=kwargs.get("base_url"),
		bank=kwargs.get("bank"),
		redirect_url=kwargs.get("redirect_url"),
		request_type="Pro",
		item_id=kwargs.get("item_id"),
		site=kwargs.get("site"),
		user=kwargs.get("user"),
		webhook_url=kwargs.get("webhook_url")
	)

def get_request_link(base_url, bank, redirect_url, request_type, item_id=None, site=None, user=None, webhook_url=None):
	bridge_user = get_bridge_user(user, site, base_url)

	request = frappe.get_doc({
		"doctype": "Bridge Item Request",
		"bridge_user": bridge_user.name,
		"request_type": request_type,
		"item_id": item_id,
		"bank_name": bank,
		"base_url": base_url,
		"redirect_url": redirect_url,
		"webhook_url": webhook_url
	}).insert(ignore_permissions=True)

	frappe.db.commit()

	return dict(
		request.get_bridge_url(),
		user=bridge_user.name
	)

@frappe.whitelist()
@handle_request_errors
def get_bridge_item_id(**kwargs):
	request = get_value("Bridge Item Request", ["bank_name", "item_id"], kwargs.get("request"), as_dict=True)

	return dict(
		bank=request.bank_name,
		item_id=request.item_id
	)

@frappe.whitelist()
@handle_request_errors
def get_bridge_item_details(**kwargs):
	item_id = kwargs.get("item_id")
	if not item_id:
		frappe.throw(_("Item ID is mandatory"))

	doc = frappe.get_doc("Bridge Item", item_id)
	doc.run_method("get_item_details")

	return doc

@frappe.whitelist()
@handle_request_errors
def delete_bridge_item_id(**kwargs):
	if not (item_id:= kwargs.get("item_id")):
		frappe.throw(_("Item ID is mandatory"))

	_get_authentication_token(kwargs.get("user"), kwargs.get("site"))

	return frappe.delete_doc_if_exists("Bridge Item", item_id, force=True)

@frappe.whitelist()
@handle_request_errors
def post_item_refresh(**kwargs):
	if not (item_id := kwargs.get("item_id")):
		frappe.throw(_("Item ID is mandatory"))

	token = _get_authentication_token(kwargs.get("user"), kwargs.get("site"))

	return BridgeAPIItem(token=token).refresh(item_id)

@frappe.whitelist()
@handle_request_errors
def get_single_bank(**kwargs):
	if not (bank_id := kwargs.get("bank_id")):
		frappe.throw(_("Bank ID is mandatory"))

	return BridgeAPIBank().get_single(bank_id)

@frappe.whitelist()
@handle_request_errors
def get_bank_accounts(**kwargs):
	item_id = kwargs.get("item_id")
	if not item_id:
		frappe.throw(_("Item ID is mandatory"))

	token = _get_authentication_token(kwargs.get("user"), kwargs.get("site"))

	return BridgeAPIAccount(token=token).get_list({"item_id": item_id})

@frappe.whitelist()
@handle_request_errors
def get_bank_transactions_by_account(**kwargs):
	if not (account_id := kwargs.get("account_id")):
		frappe.throw(_("Item ID is mandatory"))

	token = _get_authentication_token(kwargs.get("user"), kwargs.get("site"))

	return {
		"transactions": BridgeAPIAccount(token=token).get_updated_transactions_list(account_id, params=kwargs),
		"categories": BridgeAPICategory(lang=kwargs.get("lang")).get_list()
	}

@frappe.whitelist()
@handle_request_errors
def get_bank_transactions(**kwargs):
	since = None
	if kwargs.get("since"):
		since = f'{get_datetime(kwargs.get("since")).isoformat(timespec="milliseconds")}Z'

	token = _get_authentication_token(kwargs.get("user"), kwargs.get("site"))

	return {
		"transactions": BridgeAPITransaction(token=token).get_updated_list({"since": since}),
		"categories": BridgeAPICategory(lang=kwargs.get("lang")).get_list()
	}

def _get_authentication_token(user=None, site=None):
	if not (user or site):
		frappe.throw(_("User or site are mandatory"))

	bridge_user = get_bridge_user(user=user, site=site)

	return bridge_user.authenticate()


@frappe.whitelist()
@handle_request_errors
def get_category(category_id, lang="en"):
	if not category_id:
		frappe.throw(_("Category ID is mandatory"))

	return BridgeAPICategory(lang=lang).get_single(category_id)

def get_bridge_user(user=None, site=None, base_url=None):
	if user and frappe.db.exists("Bridge User", dict(
			name=user,
			status="Active"
		)
	):
		return frappe.get_doc("Bridge User", dict(
				name=user,
				status="Active"
			)
		)
	elif user and get_value("Bridge User", dict(
			name=user,
			status="Deleted"
		)
	):
		user = frappe.get_doc("Bridge User", dict(
				name=user,
				status="Deleted"
			)
		)
		user.status = "Active"
		return user.save()
	elif site and get_value("Bridge User", dict(
			site=site,
			status="Active"
		)
	):
		return frappe.get_doc("Bridge User", dict(
				site=site,
				status="Active"
			)
		)

	elif site and get_value("Bridge User", dict(
			site=site,
			status="Deleted"
		)
	):
		user = frappe.get_doc("Bridge User", dict(
				site=site,
				status="Deleted"
			)
		)
		user.status = "Active"
		return user.save()

	elif base_url:
		return frappe.get_doc({
			"doctype": "Bridge User",
			"email": f"{frappe.generate_hash(site, 8)}@dokos.cloud",
			"password": frappe.generate_hash(length=20),
			"site": site,
			"base_url": base_url
		}).insert()

	else:
		frappe.throw(_("There is no user associated with this account"))
