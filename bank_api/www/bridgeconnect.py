import frappe
from urllib.parse import urlencode

def get_context(context):
	"""
	Example form_dict
	source=connect&success=false&context=2a88edd40c&user_uuid=f17430d5-e77a-4b06-8c50-6692738b8eb7&step=email_validation_form
	source=connect&success=true&context=2a88edd40c&user_uuid=f17430d5-e77a-4b06-8c50-6692738b8eb7&step=sync_success&item_id=6497382
	"""
	args = frappe.form_dict

	if args.context:
		request = frappe.get_doc("Bridge Item Request", args.context)
		if request.uuid != args.user_uuid:
			frappe.throw("Error")

		request.status = "Completed" if args.success == "true" else "Abandonned"

		request.item_id = args.item_id

		request.step = args.step
		request.flags.ignore_permissions = True
		request.save()
		frappe.db.commit()

		if request.item_id:
			request.run_method("callback_site")

		params = {"request": request.name, "item_id": request.item_id}
		frappe.local.flags.redirect_location = f"{request.redirect_url}?{urlencode(params)}"
		raise frappe.Redirect
